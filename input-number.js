$(function() {
    $('body')
        .find('.input-number')
        .on('keydown', '.number', function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
                return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        })
        .on('change', '.number', function (e) {
            minValue =  parseInt($(this).attr('min'), 10);
            maxValue =  parseInt($(this).attr('max'), 10);
            currentVal = parseInt($(this).val(), 10);
            btnMinus = $(this).parent().find('[data-type=minus]');
            btnPlus = $(this).parent().find('[data-type=plus]');
            name = $(this).attr('name');
            if (!isNaN(minValue)) {
                if(currentVal >= minValue) {
                    btnMinus.removeAttr('disabled')
                } else {
                    $(this).val($(this).data('oldValue'));
                }
            }
            if (!isNaN(maxValue)) {
                if(currentVal <= maxValue) {
                    btnPlus.removeAttr('disabled')
                } else {
                    $(this).val($(this).data('oldValue'));
                }
            }
        })
        .on('focusin', '.number', function (e) {
           $(this).data('oldValue', $(this).val());
        })
        .on('click', '.btn-number', function() {
            var type = $(this).attr('data-type');
            var input = $(this).parents('.input-number').find('.number');
            var minValue =  parseInt(input.attr('min'));
            var maxValue =  parseInt(input.attr('max'));
            var currentVal = parseInt(input.val());
            var step = parseInt(input.attr('step'));
            input.data('oldValue', currentVal);
            if (!isNaN(currentVal)) {
                if(type == 'minus') {
                    if (isNaN(step)) {
                        input.val(currentVal - 1).change();
                    } else {
                        input.val(currentVal - step).change();
                    }
                    if(parseInt(input.val()) <= minValue) {
                        $(this).attr('disabled', true);
                    }
                } else if(type == 'plus') {
                    if (isNaN(step)) {
                        input.val(currentVal + 1).change();
                    } else {
                        input.val(currentVal + step).change();
                    }
                    if(parseInt(input.val()) >= maxValue) {
                        $(this).attr('disabled', true);
                    }
                }
            } else {
                input.val(0);
            }
        });
});
