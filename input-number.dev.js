$(function() {
    $('body')
        .find('.input-number')
        .on('keydown', '.number', function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        })
        .on('change', '.number', function (e) {
            minValue =  parseInt($(this).attr('min'), 10);
            maxValue =  parseInt($(this).attr('max'), 10);
            currentVal = parseInt($(this).val(), 10);

            btnMinus = $(this).parent().find('[data-type=minus]');
            btnPlus = $(this).parent().find('[data-type=plus]');

            name = $(this).attr('name');

            console.log("[Change] currentValue: " + currentVal);
            console.log("[Change] minValue: " + minValue);
            console.log("[Change] maxValue: " + maxValue);
            console.log("[Change] type of currentValue: " + typeof currentVal);
            console.log("[Change] type of minValue: " + typeof minValue);
            console.log("[Change] type of maxValue: " + typeof maxValue);

            console.log("[Change] currentVal >= minValue: " + (currentVal >= minValue));
            console.log("[Change] currentVal <= maxValue: " + (currentVal <= maxValue));

            if (!isNaN(minValue)) {
                if(currentVal >= minValue) {
                    btnMinus.removeAttr('disabled')
                } else {
                    console.log("[Change] minValue changed to previous value");
                    $(this).val($(this).data('oldValue'));
                }
            }
            if (!isNaN(maxValue)) {
                if(currentVal <= maxValue) {
                    btnPlus.removeAttr('disabled')
                } else {
                    console.log("[Change] maxValue changed to previous value");
                    $(this).val($(this).data('oldValue'));
                }
            }

        })
        .on('focusin', '.number', function (e) {
           $(this).data('oldValue', $(this).val());
        })
        .on('click', '.btn-number', function() {
            var type = $(this).attr('data-type');
            var input = $(this).parents('.input-number').find('.number');
            var minValue =  parseInt(input.attr('min'));
            var maxValue =  parseInt(input.attr('max'));
            var currentVal = parseInt(input.val());
            var step = parseInt(input.attr('step'));

            input.data('oldValue', currentVal);

            console.log("[input-number] current value: " + currentVal);

            if (!isNaN(currentVal)) {
                if(type == 'minus') {
                    if (isNaN(step)) {
                        input.val(currentVal - 1).change();
                    } else {
                        input.val(currentVal - step).change();
                    }

                    if(parseInt(input.val()) <= minValue) {
                        $(this).attr('disabled', true);
                    }

                } else if(type == 'plus') {
                    console.log("[input-number] Clicked: plus");

                    if (isNaN(step)) {
                        input.val(currentVal + 1).change();
                    } else {
                        input.val(currentVal + step).change();
                    }


                    if(parseInt(input.val()) >= maxValue) {
                        $(this).attr('disabled', true);
                    }
                }
            } else {
                input.val(0);
            }
        });
});
