Jquery input-number
===

Includes to html
```html
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="input-number.js"></script>
```

Example
--
```html
<div class="input-number">
    <button class="btn-number" type="button" data-type="minus">-</button>
    <input type="text" class="number" value="1">
    <button class="btn-number" type="button" data-type="plus">+</button>
</div>
```

```html
<div class="input-number">
    <button class="btn-number" type="button" data-type="minus">-</button>
    <input type="text" class="number" value="2" min="-10" max="20">
    <button class="btn-number" type="button" data-type="plus">+</button>
</div>
```
